[TOC]

**解决方案介绍**
===============
该解决方案能帮您快速实现多个无弹性公网IP的云主机安全访问公网，使用公网NAT网关服务共享弹性公网IP，节省弹性公网IP资源，同时按子网配置SNAT规则，轻松构建VPC的公网出口，避免云主机IP直接暴露在公网上。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/snat-based-public-network-connection.html](https://www.huaweicloud.com/solution/implementations/snat-based-public-network-connection.html)

**架构图**
---------------

![方案架构](./document/SNAT-based-public-network-connection.png)

**架构描述**
---------------

该解决方案会部署如下资源：
1. 创建两个子网Subnet，用于部署不同业务。
2. 创建四台弹性云服务器ECS，并绑定安全组，用于主动访问公网资源。
3. 创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。
4. 创建两个弹性公网IP，用于提供访问公网的能力。
5. 创建一个公网NAT网关，并配置SNAT规则，构建VPC中ECS的公网出口。

**组织结构**
---------------

``` lua
huaweicloud-solution-SNAT-based-public-network-connection
├── SNAT-based-public-network-connection.tf.json -- 资源编排模板
```
**开始使用**
---------------

1、查看虚拟私有云VPC实例

在[虚拟私有云VPC控制台](https://console.huaweicloud.com/vpc/?region=cn-north-4&locale=zh-cn#/vpc/vpcs/list)，可查看该方案一键生成的VPC和对应的子网/路由表/弹性服务器ECS。

图1 VPC实例
![VPC实例](./document/readme-image-001.PNG)

2、查看弹性公网IP实例

在[弹性公网IP控制台](https://console.huaweicloud.com/vpc/?region=cn-north-4#/eip/eips/list)，查看该方案一键部署创建的弹性公网IP实例。

图2 弹性公网IP实例
![弹性公网IP实例](./document/readme-image-002.PNG)


3、查看公网NAT网关实例和添加的SNAT规则

在[NAT网关页面](https://console.huaweicloud.com/vpc/?region=cn-north-4&locale=zh-cn#/nat/public/list)，查看该方案一键部署创建的公网NAT网关实例和添加的SNAT规则。

图3 公网NAT网关实例
![公网NAT网关实例](./document/readme-image-003.PNG)

图4 NAT网关的SNAT规则实例
![SNAT规则](./document/readme-image-004.PNG)

4、验证未绑定EIP的服务器是否可以通过NAT网关访问公网。

在[弹性云服务器控制台](https://console.huaweicloud.com/ecm/?region=cn-north-4#/ecs/manager/vmList)，可远程登录云服务器，通过ping IP的方式测试与公网的连通性

图5 远程登录服务器ECS
![远程登录ECS](./document/readme-image-005.PNG)

图6 snat_0001测试与公网流量互通
![snat_0001访问公网](./document/readme-image-006.PNG)

图7 snat_0011测试与公网流量互通
![snat_0011访问公网](./document/readme-image-007.PNG)


